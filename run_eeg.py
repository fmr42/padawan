from concurrent.futures import thread

import keras

import numpy as np
import logging
import sys
import time
import serial
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import TimeDistributed
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from twisted.conch.insults.insults import modes
import threading
ser = serial.Serial('/dev/arduino', 115200, timeout=1)

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

log = logging.getLogger("rec")
log.setLevel(logging.DEBUG)
pastTime_ns = 0

currentAsterix = 0
asterixsSet = ["-", "\\", "|", "/"]
model_train = keras.models.load_model('data/model/model.h5')


batch_size = 1

verbose=1
n_timesteps=16
n_features=1
n_outputs=3

model = Sequential()
model.add(LSTM(64, batch_input_shape=(batch_size,n_timesteps, n_features),stateful=True,return_sequences=True))
model.add(Dense(32, activation='softmax'))
model.add(Dropout(0.5))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

model.set_weights(model_train.get_weights())


seqList=list()
ser.flush()

threadStopSIgnal=False
s=threading.Semaphore(1)
def read_data():
    i = 0
    pastTime_ns = time.monotonic_ns()
    while True:
        if ( threadStopSIgnal ):
            print("Thread received stop sgnal, stopping...")
            return
        rawBytes = ser.readline()
        # printAsterix()
        # print(valueStr)
        try:
            recStr = (rawBytes[0:len(rawBytes) - 2].decode("utf-8"))
            rawWords = recStr.split()
            x0 = int(rawWords[0])
            y0 = int(rawWords[1])
        except:
            print("No data...")
            continue
        if not (x0 >= 0 and x0 <= 1000):
            print("Invalid data range...")
            continue
        if not (y0 >= 0 and y0 <= 100):
            print("Invalid data range...")
            continue
        s.acquire()
        seqList.append( np.asarray(x0).reshape((1,1,1)) )
        if ( len(seqList)>n_timesteps ):
            seqList.pop(0)
        s.release()
        i += 1
        if ( i%(n_timesteps*100) == 0 ):
            curTime_ns = time.monotonic_ns()
            period_ns = (curTime_ns - pastTime_ns) / n_timesteps/100
            freq=1e+9/period_ns
            print ("\nReceiving data @ " + str(int(freq)) + " Hz")
            pastTime_ns = curTime_ns
            i=0

read_data_thread = threading.Thread(target=read_data, args=(), daemon=True)
read_data_thread.start()

s.release()
while True:
    if ( len(seqList) == n_timesteps ):
        s.acquire()
        seq = np.concatenate(seqList, axis=1)/300
        seqList.pop(0)
        s.release()
        model.reset_states()
        out = model.predict(seq, batch_size=1, verbose=0, steps=None, callbacks=None, max_queue_size=10, workers=1,use_multiprocessing=False)
        cumClass = np.asarray([0, 0, 0])
        for i in range(0,out.shape[1]):
            o=out[0][i]
            c = np.where(o == np.amax(o))
            c = c[0][0]
            cumClass[c]+=1
        c = np.where(cumClass == np.amax(cumClass))
        c = c[0][0]
        print("\r", end='')
        print(c,end='')
    else:
        time.sleep(0.1)

        # curTime_ns = time.time()
        # period_ms = (curTime_us - pastTime_us) * 1000/10
        # print(period_ms)
        # print("                                            ")
        # print (period_ms, end='')
        # pastTime_us = curTime_us
