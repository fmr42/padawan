

import cv2
import brainflow
import numpy as np
from matplotlib.pyplot import plot, ion, show
from scipy import signal


import pandas as pd
import matplotlib
#from tensorflow import Round
from numpy import block
from numpy.core.tests.test_einsum import sizes

matplotlib.use ('Agg')
import matplotlib.pyplot as plt

from brainflow.board_shim import BoardShim, BrainFlowInputParams, LogLevels, BoardIds
from brainflow.data_filter import DataFilter, FilterTypes, AggOperations
#matplotlib.use('Qt5Agg')
matplotlib.use('TKAgg')

from scipy.signal import iirfilter
from scipy.signal import butter, lfilter
import matplotlib.animation as animation
import logging
import sys
import time



def appendArrayToFile(data,filename):
    f_handle = open(filename, 'a')
    np.savetxt(f_handle,data,fmt="%d")
    f_handle.close()


def writeArrayToFile(data,filename):
    f_handle = open(filename, 'w')
    np.savetxt(f_handle,data,fmt="%d")
    f_handle.close()

wannaExit=False

def key_handle(event):
    global wannaExit
    if event.key == 'escape':
        print("exit")
        wannaExit=True

BoardShim.enable_dev_board_logger ()

# use synthetic board for demo
params = BrainFlowInputParams()

params.serial_port="/dev/ttyUSB0"
# board_id = BoardIds.SYNTHETIC_BOARD.value
board_id = BoardIds.CYTON_DAISY_BOARD.value

print("--------")
print (board_id)
board = BoardShim(board_id, params)

board.prepare_session()
board.start_stream()


board.config_board('/3')
time.sleep(0.5)
board.config_board('/3')
#BoardShim.log_message (LogLevels.LEVEL_INFO.value, 'start sleeping in the main thread')
print(board.get_other_channels(board_id))
print(board.get_accel_channels(board_id))
print(board.get_analog_channels(board_id))
print(board.get_eeg_channels(board_id))
print(board.get_sampling_rate(board_id))

plt.ion() # enables interactive mode
fs=board.get_sampling_rate(board_id)
a=brainflow.data_filter.FilterTypes
nyq = 0.5 * fs
print(fs)
print("--------")
lowcut_bandpass=5
highcut_bandpass=50
low_bandpass = lowcut_bandpass / nyq
high_bandpass = highcut_bandpass / nyq
lowcut_bandstop=48
highcut_bandstop=52
low_bandstop = lowcut_bandstop / nyq
high_bandstop = highcut_bandstop / nyq
order=5
b_bandpass, a_bandpass = butter(order, [low_bandpass, high_bandpass], btype='bandpass')
b_bandstop, a_bandstop = butter(order, [low_bandstop, high_bandstop], btype='bandstop')
tmp=signal.lfilter_zi(b_bandpass, a_bandpass)
filterStateBandPass=[tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp]
tmp=signal.lfilter_zi(b_bandstop, a_bandstop)
filterStateBandStop=[tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp,tmp]

plotDuration=10#>1
loopDuration=120
dataPlotEeg=np.zeros([16,plotDuration*fs])
dataPlotDi=np.zeros([1,plotDuration*fs])
#
sTot=0
plt.clf()
fig, axs = plt.subplots(17, 1, figsize=(10, 8), constrained_layout=True,num=1)
fig.canvas.mpl_connect('key_press_event', key_handle)
fileNameX="x_eeg"
fileNameY="y_di"
writeArrayToFile([],fileNameX)
writeArrayToFile([],fileNameY)
dataRaw = board.get_board_data()
peakValues=np.ones([16,1])*100000000.0

while ( sTot<loopDuration*fs and (not(wannaExit))):
    if (board.get_board_data_count() >= plotDuration*fs):
        print("Oooops too much samples!!!!")
        dataRaw = board.get_board_data()
    elif(board.get_board_data_count()>50):
        dataRaw = board.get_board_data()
        nSamples = (dataRaw.shape[1])
        sTot=sTot+nSamples
        #print(nSamples)
        #print(dataRaw.shape)
        #print(data.shape)
        #print(data.size)
        dataEeg = np.array(dataRaw)[0:16, :]
        dataDi  = np.array(dataRaw)[21 ,:]+(np.array(dataRaw)[22 ,:])*2
        dataDiFilt=dataDi
        #print(dataEeg.shape)
        dataEegFilt0=dataEeg
        dataEegFilt1=dataEegFilt0
        dataEegFilt2, filterStateBandPass = lfilter(b_bandpass, a_bandpass, dataEegFilt1, zi=filterStateBandPass)
        dataEegFilt3, filterStateBandStop = lfilter(b_bandstop, a_bandstop, dataEegFilt2, zi=filterStateBandStop)
        dataEegFilt4=np.zeros(dataEegFilt3.shape)
        for ch in range(16):
            for s in range(dataEegFilt3.shape[1]):
                peakValues[ch] = 0.2*max(abs(dataEegFilt3[ch,s]),peakValues[ch]*0.9)+0.8*peakValues[ch]
                dataEegFilt4[ch,s]=  dataEegFilt3[ch,s]/peakValues[ch]/2.0
        dataEegFilt=dataEegFilt4
        #wrire data to file
        appendArrayToFile(dataEegFilt.transpose(), fileNameX)
        appendArrayToFile(dataDiFilt.transpose(), fileNameY)
        #appendArrayToFile(dataEegFilt.transpose(), fileNameX)
        #appendArrayToFile(dataDiFilt.transpose(), fileNameY)
        if True:
            dataPlotEeg = np.concatenate((dataPlotEeg[:, nSamples - 1:-1], dataEegFilt), axis=1)
            dataPlotDi = np.concatenate((dataPlotDi[:, nSamples - 1:-1], dataDiFilt.reshape([1,dataDiFilt.size])), axis=1)
            #print(dataPlot.shape)
            #dataBoardLen=data.shape[2]
            for f in range(16):
                axs[f].axes.cla()
                axs[f].set_ylim(-1.1,1.1)
                axs[0].set_xticklabels([])
                axs[f].plot(dataPlotEeg[f,:].transpose())
            axs[16].axes.cla()
            axs[16].set_ylim(-0.5, 1.5)
            axs[16].plot(dataPlotDi.transpose())
            #axs[0].set_yscale('linear')
            #axs[0].set_title('')
            #plt.gca().axes.get_yaxis().set_visible(False)
            #axs[0].grid(True)
            #axs[1].grid(True)
            # plt.plot(dataPlotEeg.transpose()) #input 11 12
            #plt.plot(dataPlot.transpose()) #input 11 12
            plt.pause(0.001)
            plt.show(block=False)

board.stop_stream ()
board.release_session ()


