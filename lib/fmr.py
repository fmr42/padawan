# lstm model
from keras.backend import batch_dot
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense, Embedding, TimeDistributed, Conv1D, MaxPooling1D
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.utils import to_categorical
from matplotlib import pyplot
import os
import keras
from keras.layers import TimeDistributed
import cv2.cv2 as cv2

from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import TimeDistributed
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from matplotlib import pyplot
from keras.layers import ConvLSTM2D

def load_model(filePath):
    return ( keras.models.load_model(filePath))

def build_model(inputShape,n_outputs):
    model = Sequential()
    model.add(LSTM(64, batch_input_shape=inputShape,stateful=True,return_sequences=True))
    model.add(Dense(32, activation='softmax'))
    model.add(Dropout(0.5))
    model.add(Dense(n_outputs, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

def build_model_load_weights(inputShape,weightsFilePath):
    model_weights=keras.models.load_model(weightsFilePath)
    model = build_model(inputShape)
    model.set_weights(model_weights.get_weights())
    return model

