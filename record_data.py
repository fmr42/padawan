
import numpy as np
import logging
import sys
import time
import serial





def printAsterix():
    global asterixsSet
    global currentAsterix
    print(asterixsSet[currentAsterix],end='        ')
    currentAsterix = currentAsterix + 1
    if ( currentAsterix>= len(asterixsSet) ):
        currentAsterix=0

def appendArrayToFile(data,filename):
    f_handle = open(filename, 'a')
    np.savetxt(f_handle,data,fmt="%d")
    f_handle.close()

x=[]
y=[]

ser = serial.Serial('/dev/arduino',115200,timeout=1)

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

log = logging.getLogger("rec")
log.setLevel(logging.DEBUG)
pastTime_ns=time.monotonic_ns()


currentAsterix=0
asterixsSet=["-","\\","|","/"]

i=0
while True :
    i=i+1
    rawBytes=ser.readline()
    #printAsterix()
    # print(valueStr)
    try :
        recStr = (rawBytes[0:len(rawBytes) - 2].decode("utf-8"))
        rawWords = recStr.split()
        x0 = int(rawWords[0])
        y0 = int(rawWords[1])
    except:
        print("No data...", end='')
        continue
    if not (x0 >= 0 and x0 <= 1000):
        continue
    if not (y0 >= 0 and y0 <= 100):
        continue
    batch_size=256
    # 16 sec @ 256Hz = 4096
    # 16 sec @ 16Hz  = 256


    if ( len(x)>0 and len(x) < batch_size ):
        x = np.append(x, [[x0]], axis=0)
        y = np.append(y, [[y0]], axis=0)
    elif (len(x)==0):
        x=np.array([[x0]])
        y = np.array([[y0]])
    elif ( len(x)>=batch_size ):
        print("-----------------")
        print("Saving data....")
        appendArrayToFile(x, "data/x")
        appendArrayToFile(y, "data/y")
        x = np.array( [[x0]])
        y = np.array([[y0]])
        curTime_ns = time.monotonic_ns()
        period_ns = (curTime_ns - pastTime_ns)
        print("period ns batch = " + str(int(period_ns)))
        print("freq = " + str((1000000000.0/period_ns)*batch_size))
        pastTime_ns = curTime_ns



