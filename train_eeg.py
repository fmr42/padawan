

# lstm model
from keras.backend import batch_dot
#from keras.metrics import accuracy
from numpy import mean
from numpy import std
from numpy import dstack
import numpy as np
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense, Embedding, TimeDistributed, Conv1D, MaxPooling1D
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.utils import to_categorical
from matplotlib import pyplot
import os
import keras
from keras.layers import TimeDistributed
import cv2.cv2 as cv2

from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import TimeDistributed
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.utils import to_categorical
from matplotlib import pyplot
from keras.layers import ConvLSTM2D


import lib.fmr as fmr


import  matplotlib
# load a single file as a numpy array
#from tensorflow_core.python.ops.gen_stateful_random_ops import stateful_uniform_full_int
from run import n_features
from train import n_samples, n_features

matplotlib.use('QT5Agg')


def load_file(filepath):
    print ("Loading " + filepath)
    dataframe = read_csv(filepath, header=None, delim_whitespace=True)
    return dataframe.values

# load a list of files and return as a 3d numpy array
def load_group(filenames):
    loaded = list()
    for name in filenames:
        data = load_file(name)
        loaded.append(data)
    # stack group so that features are the 3rd dimension
    loaded = dstack(loaded)
    return loaded

# load a dataset group, such as train or test
def load_dataset_group(group, prefix=''):
    # load all files as a single array
    filepath = prefix + group + '/x/'
    filenames = list()
    # r=root, d=directories, f = files
    for r, d, f in os.walk(filepath):
        for file in f:
            if '.txt' in file:
                filenames.append(os.path.join(r, file))
    X = load_group(filenames)
    # load class output
    y = load_file(prefix + group + '/y.txt')
    return X, y





# load the dataset, returns train and test X and y elements
def load_dataset(prefix=''):
    # load all train
    trainX, trainy = load_dataset_group('train', prefix + 'data/dataset/')
    print("trainX.shape")
    print(trainX.shape)
    print("trainy.shape")
    print(trainy.shape)
    # load all test
    testX, testy = load_dataset_group('test', prefix + 'data/dataset/')
    print("textX.shape")
    print(testX.shape)
    print("texty.shape")
    print(testy.shape)
    trainy = to_categorical(trainy)
    testy = to_categorical(testy)
    print(trainX.shape, trainy.shape, testX.shape, testy.shape)
    return trainX, trainy, testX, testy

def fixXData(d,n_timesteps):
    d_cut=d[0:int(d.shape[0] / n_timesteps) * n_timesteps, :, :]
    d_reshaped=d_cut.reshape([d_cut.shape[0],d_cut.shape[1]])
    return d_reshaped

def fixYData(d,n_timesteps):
    d_cut=d[0:int(d.shape[0] / n_timesteps) * n_timesteps, :]
    d_reshaped=d_cut
    return d_reshaped

# fit and evaluate a model
n_timesteps = 512    # 128*4
trainXRaw, trainYRaw, testXRaw, testYRaw = load_dataset()
trainX = fixXData(trainXRaw,n_timesteps)
testX = fixXData(testXRaw,n_timesteps)
trainY = fixYData(trainYRaw,n_timesteps)
testY = fixYData(testYRaw,n_timesteps)
trainXRaw.shape
trainXRaw.shape
trainX.shape




verbose=1
epochs=10
# #TODO n_lags
# xList=list()
# #trainX=np.arange(64).reshape((64,1,1))
# yList=list()
#
# iMax=trainX.shape[0]-n_timesteps+1
# for i in range ( 0 , iMax ):
#     if ( i%int(iMax/5) == 0 ):
#         print(str(int(i/iMax*100))+" %")
#     seq=np.reshape(np.asarray(trainX[i:i+n_timesteps]) , (1,n_timesteps,n_features) )
#     xList.append(seq)
#     yList
# x=np.concatenate(xList,axis=0)
n_samples=int(trainX.shape[0]/n_timesteps)
n_features=trainX.shape[1]
n_outputs=trainY.shape[1]
x=trainX.reshape(n_samples,n_timesteps,n_features)
y=trainY.reshape(n_samples,n_timesteps,n_outputs)


model = Sequential()
model.add(LSTM(100, input_shape=(n_timesteps, n_features)))
model.add(Dropout(0.5))
model.add(Dense(100, activation='relu'))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

batch_size = n_samples
n_batch   = int( n_samples/batch_size )
input_shape=(batch_size,n_timesteps, n_features)
# define model

model=fmr.build_model(input_shape,n_outputs)
#model=fmr.load_model('data/model/model.h5')
verbose=2
e=0
_, maxAccuracy = model.evaluate(x, y, batch_size=batch_size, verbose=verbose)
saveModel=False
while(True):
    e+=1
    if ( e%100==0 ):
        print("-- Epoch " + str(e) )
    model.reset_states()
    model.fit(x, y, epochs=1, batch_size=batch_size, verbose=verbose,shuffle=False,workers=2)
    _, accuracy = model.evaluate(x, y, batch_size=batch_size, verbose=verbose)
    if(accuracy>maxAccuracy):
        print(":) Accuracy " + str(accuracy) + " > " + str(maxAccuracy))
        if (saveModel):
            print("   Saving model...")
            model.save('data/model/model.h5')
        maxAccuracy = accuracy

# for i in range(epochs):
#     print('Epoch', i + 1, '/', epochs)
#     # Note that the last state for sample i in a batch will
#     # be used as initial state for sample i in the next batch.
#     # Thus we are simultaneously training on batch_size series with
#     # lower resolution than the original series contained in data_input.
#     # Each of these series are offset by one step and can be
#     # extracted with data_input[i::batch_size].
#     model_stateful.fit(x_train,
#                        y_train,
#                        batch_size=batch_size,
#                        epochs=1,
#                        verbose=1,
#                        validation_data=(x_test, y_test),
#                        shuffle=False)
#     model_stateful.reset_states()
#
#
#
# model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=verbose)
# # evaluate model
# _, accuracy = model.evaluate(testX, testy, batch_size=batch_size, verbose=0)
#
# #adam = Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
# model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# # fit network
# model.fit(trainX, trainy, epochs=1, batch_size=batch_size, verbose=verbose,shuffle=False)
# model.save('model/model.h5')
# # evaluate model
# #
# model=keras.models.load_model('model/model.h5')
# _, accuracy = model.evaluate(testX, testy, batch_size=batch_size, verbose=verbose)
# #for n in range(trainX.shape[0]):
# #p=model.predict(trainX, batch_size=None, verbose=0, steps=None, callbacks=None, max_queue_size=10, workers=1,use_multiprocessing=False)
# for i in range(1):
#     r=np.where(p[i] == np.amax(p[i]))
#     r=r[0][0]
#     s = np.where(trainy[i] == np.amax(trainy[i]))
#     s = s[0][0]
#     dat=trainX[i]
#     dat=dat.reshape((1,dat.shape[0],dat.shape[1]))
#     rtp=model.predict(dat, batch_size=None, verbose=0, steps=None, callbacks=None, max_queue_size=10, workers=1,use_multiprocessing=False)
#
#     t = np.where(rtp == np.amax(rtp))
#     t=t[1][0]
#     print(str(r) + "  " + str (s)+ "  " + str (t) )
#
# print(accuracy)
#
#
#
# #for i in range(trainy.shape[0]):
# #    print(trainy[i])
#rtp=model.predict(x, batch_size=None, verbose=0, steps=None, callbacks=None, max_queue_size=10, workers=1,use_multiprocessing=False)
