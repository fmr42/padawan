
const double interruptFreq = 64 ;

const unsigned int intTimerPreLoad = 65536.0 - (16000000.0 / 256.0 / interruptFreq) ;

int timeTrigger=0;
ISR(TIMER1_OVF_vect)        // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  TCNT1 = intTimerPreLoad;            // preload timer
  timeTrigger=1;
}

void setup() {
  Serial.begin(115200); // Starting Serial Terminal
  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);
  pinMode(10, INPUT_PULLUP);
  // initialize timer1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1 = intTimerPreLoad;            // preload timer 65536-16MHz/256/2Hz
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts

}


void loop() {
  int txSubSamp=16;
  if ( timeTrigger>0 ) {
    int y0;
    timeTrigger=0;
    if ( !digitalRead(10) ){
      y0=1;
    }else if ( !digitalRead(9) ){
      y0=2;
    }else if ( !digitalRead(8) ){
      y0=3;
    } else {
      y0=0;
    }
    Serial.println(y0);
  }
}
